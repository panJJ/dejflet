﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatManager : MonoBehaviour
{
    public GameObject Beat;
    public List<GameObject> SpawnedBeats;
    public float radius;
    public int BeatCount;
    // Start is called before the first frame update
    void Start()
    {
        radius = transform.localScale.x;
        float BeatAngle = 360 / BeatCount;

        for (int i = 0; i < BeatCount; i++)
        {
            float angle = i * Mathf.PI * 2f / BeatCount;
            Vector3 newPos = transform.position + new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius, 0 );
            SpawnedBeats.Add(Instantiate(Beat, newPos, Quaternion.identity, transform));
        }
    }

    // Update is called once per frame
    void Update()
    {
        RotateBeats();
    }

    void RotateBeats()
    {

        foreach (GameObject beat in SpawnedBeats)
        {
            beat.transform.position = new Vector2(transform.position.x, transform.position.y)+ new Vector2(0f + (10f * MCos(alpha) * MCos(tilt)) - (5f * MSin(alpha) * MSin(tilt)),
                                          0f + (10f * MCos(alpha) * MSin(tilt)) + (5f * MSin(alpha) * MCos(tilt)));
            alpha += 1f;
        }
    }

    public float alpha = 0f;

    public float tilt = 45f;


    float MCos(float value)
    {
        return Mathf.Cos(Mathf.Deg2Rad * value);
    }

    float MSin(float value)
    {
        return Mathf.Sin(Mathf.Deg2Rad * value);
    }
}
